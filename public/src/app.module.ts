//import { NgModule }      from '@angular/core';
import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent }  from './app.component';
import { MaterialModule } from '@angular/material';


//import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';

@NgModule({
  imports: [ 
    BrowserModule, 
    NgbModule.forRoot(), 
    FormsModule, 
    ReactiveFormsModule, 
    JsonpModule, 
    MaterialModule    
  ],
  declarations: [ 
    AppComponent    
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }


